<table id="management_visit_report" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline collapsed">
    <thead>
        <tr>
            <th nowrap>#</th>
            <th nowrap>idcard</th>
            <th nowrap>hn</th>
            <th nowrap>refno</th>
            <th nowrap>มีขั้นตอนคัดกรอง</th>
            <th nowrap>ผ่านคัดกรอง</th>
            <th nowrap>userที่คัดกรอง</th>
            <th nowrap>idห้องตรวจ</th>
            <th nowrap>ชื่อห้องตรวจ</th>
            <th nowrap>idของอาคาร</th>
            <th nowrap>ชื่ออาคาร</th>
            <th nowrap>วันที่</th>
            <th nowrap>เปิดvisitสำเร็จ</th>
            <th nowrap>visitno</th>
            <th nowrap>userที่เปิดvisit</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (isset($Data) && count($Data) > 0) :
            foreach ($Data as $key => $value) {
        ?>
                <tr>
                    <td><?=$key+1;?></td>
                    <td><?=$value->idcard;?></td>
                    <td><?=$value->hn;?></td>
                    <td><?=$value->refno;?></td>
                    <td><?=$value->selectqueue?'ใช่':'ไม่ใช่';?></td>
                    <td><?=$value->closequeue?'ใช่':'ไม่ใช่';?></td>
                    <td><?=$value->closeuser;?></td>
                    <td><?=$value->room_uid;?></td>
                    <td><?=$value->room_name;?></td>
                    <td><?=$value->building_uid;?></td>
                    <td><?=$value->buildingname;?></td>
                    <td><?=$value->cwhen;?></td>
                    <td><?=$value->api_status_name;?></td>
                    <td><?=isset(json_decode($value->api_status_desc,TRUE)['visitNo'])?json_decode($value->api_status_desc,TRUE)['visitNo']:'';?></td>
                    <td><?=$value->api_cuser;?></td>
                </tr>
        <?php
            }
        endif;
        ?>
    </tbody>
</table>