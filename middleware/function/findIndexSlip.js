module.exports = function findIndexSlip(location,kiosk_location) {
    var index = -1;
    global.endkiosk.find(function(item, i){
        if(item.location == location && item.kiosk_location == kiosk_location){
            index = i;
            return i;
        }
    });
    return index;
}