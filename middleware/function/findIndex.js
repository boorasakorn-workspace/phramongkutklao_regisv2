module.exports = function findIndex(location,kiosk_location) {
    // //console.log(location, 'TEST LOCATION')
    var index = -1;
    global.kiosk.find(function(item, i){
        if(item.location == location && item.kiosk_location == kiosk_location){
            index = i;
            return i;
        }
    });
    return index;
}

