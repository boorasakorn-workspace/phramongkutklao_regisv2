const { Pool } = require('pg');
const findIndex = require('./../function/findIndex');
//const config = require('./../function/connect');

const axios = require('axios');
const pool = new Pool(config['dbconfig']);
var dateFormat = require('dateformat');
const fs = require('fs');

module.exports = function getPayorByType(type, idcard, hn, kiosk_location, location) {
    // //console.log(idcard,'chkTypePayor');
    clearKioskLocation({ kiosk_location: kiosk_location, location: location });
    // //console.log(idcard, 'PID')
    // //console.log(type, 'TYPE')
    // //console.log(hn, 'HN')

    var d = new Date();
    var now = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
    var datetime = dateFormat(now, "yyyy-mm-dd");


    pool.query("SELECT uid,code FROM tb_patienttype WHERE code='" + type + "' ORDER BY  public.tb_patienttype.order", (err, results) => {
        if (!err) {
            if (results.rowCount > 0) {
                // //console.log(results.rows);
                for (var i = 0; i < results.rows.length; i++) {
                    var uidType = results.rows[i].uid;
                }
                var data2 = {};
                data2.location = '4';
                pool.query("SELECT * FROM tb_payor ORDER BY public.tb_payor.order ASC", (err, results) => {
                    data2.Payor = results.rows;
                    // //console.log(data2, 'data2');
                });

                // //console.log(idcard, 'GET idcard PAYOR')
                pool.query("SELECT patienttypeid,idcard,payorid FROM tr_patient WHERE patienttypeid='" + uidType + "' AND idcard='" + idcard + "' GROUP BY payorid,patienttypeid,idcard", (err, results) => {
                    data2.Patient = results.rows;
                    // //console.log(data1, 'data1');
                });
                data2.kiosk_location = kiosk_location;
                // //console.log(hn, "HN FOR GET LAST")
                if (hn != '' && hn != '-') {
                    getLastPayorByHN({ hn: hn, datetime: datetime, idcard: idcard, kiosk_location: kiosk_location }, (res_getlastpayor) => {
                        // //console.log(res_getlastpayor, "GET LAST PAYOR !!!!")
                        if (res_getlastpayor.success) {
                            //console.log(res_getlastpayor, "GET LAST PAYOR 3")
                            if (typeof res_getlastpayor.data != undefined) {
                                data2.dataLastPayor = res_getlastpayor.data;
                            } else {
                                data2.dataLastPayor = '';
                            }

                            if (res_getlastpayor.data1.length > 0) {
                                data2.dataLastPayorAll = res_getlastpayor.data1;
                            } else {
                                data2.dataLastPayorAll = '';
                            }
                            global.kiosk.push(data2);
                            //console.log(global.kiosk, "GET LAST PAYOR 1")
                        } else if (res_getlastpayor.success && res_getlastpayor.error) {
                            //console.log(res_getlastpayor, "GET LAST PAYOR 4")
                            data2.dataLastPayor = res_getlastpayor.data;
                            data2.dataLastPayorAll = res_getlastpayor.data1;
                            global.kiosk.push(data2);
                            //console.log(global.kiosk, "GET LAST PAYOR 2")
                        }
                    });
                } else {
                    data2.dataLastPayor = { success: true, data: '' };
                    data2.dataLastPayorAll = { success: true, data: '' };
                    global.kiosk.push(data2);
                    // //console.log(global.kiosk, "GET LAST PAYOR 1")
                }

            } else {
                // //console.log('No Data Valid');
            }
        }

    });
}

// var getLastPayorByHN = (param, callback) => {
//     //console.log(param, "PARAMETER GET LAST PAYOR")
//     if(param.hn){
//         fs.readFile('./mockupjson/onsite-pmk/api-currentcredit.json', 'utf8', (err, dataJson) => {
//             if (err) {
//                 //console.log("File read failed:", err)
//                 callback({success: true, data: ''})
//             }
//             var aaa = JSON.parse(dataJson.toString());
//             const arr2 = aaa.filter((d) => {
//                 return d.hn == param.hn;
//             });
//             //console.log(aaa);
//             if(arr2.length){
//                 callback({success: true, data: arr2[arr2.length - 1],data1: arr2})
//             }else if(arr2.length == 1){
//                 callback({success: true, data: arr2, data1: arr2})
//             }else{
//                 callback({success: true, data: '', data1: ''})
//             }

//         });
//     }


// }

var getLastPayorByHN = (param, callback) => {
    console.log(param, "get last payor by hn")
    var fullHn = null;
    var s = param.hn.split('/');
    var idcard = param.idcard;
    var kiosk_location = param.kiosk_location;
    fullHn = s[1] + '/' + s[0];
    // //console.log(fullHn, "Payor")
    var productpayorurl = `${config['hlabAPI']}/ords/pmkords/hlab/credit/${fullHn}/null/${param.datetime}`;

    console.log(productpayorurl, "Payor")
    axios({
        method: "get",
        url: productpayorurl,
        headers: {
            "Content-Type": "application/json"
        },
    }).then(function (response) {
        get_credit_master(fullHn, kiosk_location);
        // //console.log(response, "GET DATA LAST PAYOR RESPONSE")
        //insert_status_payor(response.data, response.status, 'true', kiosk_location);
        if (response.status == 200 || response.status == 201) {
            if (response.data.length > 0) {
                // //console.log("OK STATUS")
                // //console.log(response.data, "GET DATA RESPONSE")
                callback({ success: true, data: response.data[response.data.length - 1], data1: response.data });
            } else {
                callback({ success: true, data: '', data1: response.data });
            }
        }
    }).catch(error => {
        get_credit_master(fullHn, kiosk_location);
        console.log('errorerror');
        console.log(error);
        //insert_status_payor({ hn: param.hn, pid: idcard }, error.response.status, 'false', kiosk_location);
        if (error.response.status == 404) {
            callback({ success: true, data: '', data1: '' });
        } else if (error.response.status == 500) {
            callback({ success: true, error: 500, data: '', data1: '' });
        }
        // //console.log(err.response.status, "ERROR API FROM APPOINTMENT WITH INPUT")
    });
}

var clearKioskLocation = (param) => {
    const arr1 = global.kiosk.filter((d) => {
        return d.kiosk_location != param.kiosk_location && d.location == param.location;
    });
    global.kiosk = arr1;
}

var get_credit_master = (fullHn, kiosk_location) => {

    var productpayorurl = `${config['hlabAPI']}/ords/pmkords/hlab/credit-master/${fullHn}`;

    axios({
        method: "get",
        url: productpayorurl,
        headers: {
            "Content-Type": "application/json"
        },
    }).then(function (response) {
        console.log('credit_master_success');
        insert_status_payor(response.data, '200', 'true', kiosk_location);
    }).catch(error => {
        console.log('credit_master_fail');
        insert_status_payor(response.data, '200', 'false', kiosk_location);
    });



}


var insert_status_payor = (data, statusapi, status_name, kiosk_location) => {
    // pool.query('', (err, result) => {

    // });
    console.log('ststus_payor 222123');
    console.log(data);
    var data_main = JSON.stringify(data);

    try {
        var hn = (typeof data[0]['hn'] == 'undefined') ? data['hn'] : data[0]['hn'];
        var idcard = (typeof data[0]['pid'] == 'undefined') ? data['hn'] : data[0]['pid'];
        var cuser = kiosk_location;
        var refno = null;
        var status_api = statusapi;
        var status_get = statusapi;
        var system_type = "kiosk";
        var status_name = status_name;
    } catch (e) {
        var hn = data['hn'];
        var idcard = data['pid'];
        var cuser = kiosk_location;
        var refno = null;
        var status_api = statusapi;
        var status_get = statusapi;
        var system_type = "kiosk";
        var status_name = status_name;
    }




    pool.query(`INSERT INTO tr_api_status(hn,idcard,cuser,refno,status_api,status_get,system_type,status_name) VALUES('${hn}',${idcard},${cuser},${refno},${status_api},${status_get},'${system_type}','${status_name}') returning uid`, (err, result) => {
        if (err) {
            console.log('result insert error');
            console.log(err);
        } else {
            console.log(data_main);
            console.log('eueu');
            var result_id = parseInt(result.rows[0]['uid']);
            pool.query(`INSERT INTO tr_api_detail(apistatusuid,datajson) VALUES(${result_id},'${data_main}')`, (err2, result2) => {
                if (err2) {
                    console.log('insert error222');
                    console.log(err2);
                } else {
                    console.log('result insert22');
                }
            });
        }
    });

    //console.log(data);
}
