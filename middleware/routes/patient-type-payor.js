const router = require('express').Router();
const { Pool } = require('pg');
const findIndex = require('./../function/findIndex');
const getPayorByType = require('./../function/getPayorByType');
//const config = require('./../function/connect');

const axios = require('axios');
const pool = new Pool(config['dbconfig']);



router.post('/type', (req, res) => {

    clearKioskLocation(req.body.kiosk_location);

    // //console.logindex,'testindex')
    // //console.logreq.body.idcard, 'TEST ID CARD for get Type');
    if (req.body.requestType == '1') { // Get Round 1 Patient Type Data
        pool.query("SELECT * FROM tb_patienttype ORDER BY public.tb_patienttype.order ASC", (err, results) => {
            if (!err) {
                if (results.rowCount > 0) {
                    // //console.logreq.body, "test data type")
                    // //console.logresults.rows, "test data type 1")
                    res.status(200).send({ success: true, step: 'next4', data: results.rows, from_lc: '3', kiosk_location: req.body.kiosk_location });

                } else {
                    // //console.log'No Data Valid');
                    // var data = {location:'1',data:req.body,step:'next2',scan:true};
                    // global.kiosk.push(data);
                    // global.io.emit('kiosk_hn', data);
                }
                return results.rows;
            } else {
                res.status(200).send({ success: false, step: 'none', kiosk_location: req.body.kiosk_location });

            }

        });
    } else if (req.body.requestType == '2') { // Get Round 2 Get Patient Type & Payor
        
        // //console.logreq.body, "TYPE 2")
        if ((req.body.typeval) && (req.body.idcard != '' || req.body.hn_no != '' || req.body.passport != '')) {
            // //console.logreq.body, "type payor requestType 2");
            // if(req.body.hn == ''){
            //     var hn = '';
            // }
             getPayorByType(req.body.typeval, req.body.idcard, req.body.hn, req.body.kiosk_location, req.body.location);
             res.status(200).send({ success: true, kiosk_location: req.body.kiosk_location });
            
        }
        // if((req.body.typeval == '001') && (req.body.idcard != '' || req.body.hn_no != '' || req.body.passport != '')){
        //     // //console.log'001');
        //     getPayorByType(req.body.typeval,req.body.idcard);
        //     res.status(200).send({success:true});
        // }else if(req.body.typeval == '0002'){
        //     // //console.log'002');
        //     getPayorByType(req.body.typeval,req.body.idcard);
        //     res.status(200).send({success:true, step:'next4'});
        // }else if((req.body.typeval == 'A01') && (req.body.idcard != '' || req.body.hn_no != '' || req.body.passport != '')){
        //     // //console.log'003');
        //     getPayorByType(req.body.typeval,req.body.idcard);
        //     res.status(200).send({success:true});
        // }else if((req.body.typeval == '004') && (req.body.idcard != '' || req.body.hn_no != '' || req.body.passport != '')){
        //     // //console.log'004');
        //     getPayorByType(req.body.typeval,req.body.idcard);
        //     res.status(200).send({success:true});
        // }else if((req.body.typeval == '005') && (req.body.idcard != '' || req.body.hn_no != '' || req.body.passport != '')){
        //     // //console.log'005');
        //     getPayorByType(req.body.typeval,req.body.idcard);
        //     res.status(200).send({success:true});
        // }else if((req.body.typeval == '006') && (req.body.idcard != '' || req.body.hn_no != '' || req.body.passport != '')){
        //     // //console.log'006');
        //     getPayorByType(req.body.typeval,req.body.idcard);
        //     res.status(200).send({success:true});
        // }else if((req.body.typeval == '007') && (req.body.idcard != '' || req.body.hn_no != '' || req.body.passport != '')){
        //     // //console.log'007');
        //     getPayorByType(req.body.typeval,req.body.idcard);
        //     res.status(200).send({success:true});
        // }
    }
});

router.post('/payor/fetch', (req, res) => { // Return Get Patient Type & Payor
    // //console.logglobal.kiosk, 'test global');
    var valueIndex = [];
    global.kiosk.find(function (item, i) {
        if (item.location == req.body.location && item.kiosk_location == req.body.kiosk_location) {
            valueIndex.push(global.kiosk[i]);
        }
    });
    if (req.body.location == '4' && req.body.step == 'next4') {
        //console.log(valueIndex);
        // //console.log(valueIndex, "TEST DATA NEXT4")
        res.status(200).send({ success: true, data: valueIndex, kiosk_location: req.body.kiosk_location,check:global.kiosk  });
        //console.log{ data: valueIndex }, 'test Data Mock 2 Data');
        // global.io.emit('update_management', {data:valueIndex});
        // setTimeout(function () {
        //     //console.logvalueIndex, "TEST MOCK IN TIMEOUT");
        //     global.io.emit('payor_data', { data: valueIndex, kiosk_location: req.body.kiosk_location });
        // }, 1000);
    } else {
        res.status(200).send({ success: false, message: 'No Data' ,check:global.kiosk});
    }
})

var clearKioskLocation = (param) => {
    const arr1 = global.kiosk.filter((d) => {
        return d.kiosk_location != param;
    });
    global.kiosk = arr1;
}


module.exports = router;