const router = require('express').Router();
const { Pool } = require('pg');
var dateFormat = require('dateformat');
//const config = require('./../function/connect');
const findIndexSlip = require('../function/findIndexSlip');
const deleteIndexAll = require('./../function/deleteIndexAll');
var qs = require('qs');

const axios = require('axios');
const pool = new Pool(config['dbconfig']);



router.post('/print', (req, res) => {
    //console.log(req.body, "TEST PRINT ROUTER")
    // var index = findIndexSlip(7);
    // if (index != -1) {
    //     global.endkiosk.splice(index, 1);
    // }

    // //console.logglobal.endkiosk, "TEST INDEX KIOSK")
    if (req.body.requestPrint == '1') {
        var refnoxx = req.body.refno;
        //console.log(refnoxx, "PRINT PRINT");
        var listgroupname = '';
        if (req.body.worklistgroup == '1') {
            listgroupname = 'Walk-in';
        } else if (req.body.worklistgroup == '2') {
            listgroupname = 'นัดหมาย';
        } else if (req.body.worklistgroup == '3') {
            listgroupname = 'ผู้ป่วยใหม่';
        } else if (req.body.worklistgroup == '4') {
            listgroupname = 'Walk-in';
        } else if (req.body.worklistgroup == '5') {
            listgroupname = 'นัดหมาย';
        } else if (req.body.worklistgroup == '6') {
            listgroupname = 'นัดหมาย';
        } else if (req.body.worklistgroup == '7') {
            listgroupname = 'นัดหมาย';
        } else if (req.body.worklistgroup == '8') {
            listgroupname = 'Walk-in';
        } else if (req.body.worklistgroup == '9') {
            listgroupname = 'นัดหมาย';
        } else if (req.body.worklistgroup == '10') {
            listgroupname = 'นัดหมาย';
        }

        if (req.body.Queueno != '' && req.body.Categoryuid != '' && req.body.worklistgroup != '') {
            //console.log(req.body, 'TEST DATA PRINT QUEUE')
            var data = req.body;
            var dataInfo = '';
            // ////console.logdata, 'printQueue');
            // ////console.logJSON.parse(req.body.test));
            if (req.body.info) {
                dataInfo = JSON.parse(req.body.info);
                // //console.logdataInfo, 'INFO');
            }
            var Queueno = JSON.parse(req.body.Queueno);
            var dataCate = JSON.parse(req.body.Categoryuid);
            //console.logdata, 'Data Print Slip');
            //console.logdataCate, 'TEST CATE 0');
            //console.logdataCate[0], 'TEST CATE 1');
            //console.logdataCate[1], 'TEST CATE 2');
            //console.logQueueno, "TEST QUEUE");

            // var sql ="select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,max(queueno) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title from tr_patient c LEFT JOIN tr_patientqueue a ON c.uid = a.patientuid left join vw_worklistprocess b on(b.worklistgroup = '" + data.worklistgroup + "' and(b.groupprocessuid = a.groupprocessuid or b.groupprocessuid is null)) where c.uid = '" + data.Patientuid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle Order by worklistuid"
            var sql = "select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,(select queueno from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and tr_patientqueue.groupprocessuid = max(b.groupprocessuid) limit 1) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title, ( select tb_payor.counter from tb_payor where tb_payor.uid = c.payorid::integer limit 1 ) as counterpayor from vw_worklistprocess b LEFT outer JOIN tr_patient c on b.worklistgroup = '" + data.worklistgroup + "' left outer join tr_patientqueue a ON c.uid = a.patientuid where c.uid = '" + data.Patientuid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle,a.groupprocessuid,c.payorid Order by worklistuid";
            //console.log(sql, "SQL");
            pool.query(sql, (err, results) => {
                // console.log(results, "results");
                // console.log(err, "err");
                if (!err) {
                    if (results.rowCount > 0) {
                        // //console.logresults.rows, "TEST SELECT QUERY")
                        var aaa = [];
                        var sql_value = "";
                        var fullname = "";
                        var birthdate = "";
                        var patientuid = "";
                        var citizenid = "";
                        var hn = "";
                        var regiswhen = "";
                        var flowname = '';
                        var queueno = "";
                        var flownameRoom = "";

                        clearKioskLocation(req.body.kiosk_location);

                        for (var i = 0; i < results.rows.length; i++) {
                            if (results.rows[i].groupprocessuid) {
                                sql_value = sql_value + ",('" + results.rows[i].patientuid + "','" + results.rows[i].worklistuid + "',now(),'" + results.rows[i].queueno + "')"
                            } else {
                                sql_value = sql_value + ",('" + results.rows[i].patientuid + "','" + results.rows[i].worklistuid + "',now(),NULL)"
                            }

                            // //console.log(results.rows[i].clinicname, "TEST CLINIC NAME");
                            // if(results.rows[i].clinicname != ''){
                            //     if(results.rows[i].clinicname.includes('.')){
                            //         clinicname = results.rows[i].clinicname.replace(".",",");
                            //     }else{
                            //         clinicname = results.rows[i].clinicname;
                            //     }
                            // }


                            if (results.rows[i].worklistuid == 6 && results.rows[i].clinicname != '') {
                                flownameRoom = ' ' + results.rows[i].clinicname;//.replace(".",",");
                            }
                            //console.log(flownameRoom, "TEST CLINIC NAME");
                            var inFoData2 = '';
                            if (dataInfo.data2) {
                                inFoData2 = dataInfo.data2;
                            }
                            results.rows[i].allcounter = (results.rows[i].worklistuid == 5) ? results.rows[i].counterpayor : results.rows[i].allcounter;
                            var dddd = {
                                "location": '7',
                                "sequence": i + 1,
                                "title": results.rows[i].title,
                                "flowname": results.rows[i].flowname + flownameRoom,
                                "waitinghn": results.rows[i].count_patient_newhn,
                                "waitingpayor": results.rows[i].count_patient_payor,
                                "queueno": (results.rows[i].groupprocessuid ? results.rows[i].queueno : ''),
                                "worklistuid": results.rows[i].worklistuid,
                                "groupprocessuid": results.rows[i].groupprocessuid,
                                "infoData": inFoData2,
                                "group": results.rows[i].worklistgroup,
                                "closeselectone": results.rows[i].closeselectone,
                                "closevs": results.rows[i].closevs,
                                "closenewhn": results.rows[i].closenewhn,
                                "closepayor": results.rows[i].closepayor,
                                "count_patient_newhn": results.rows[i].count_patient_newhn,
                                "count_patient_payor": results.rows[i].count_patient_payor,
                                "cancelregister": results.rows[i].cancelregister,
                                "cancelnewhn": results.rows[i].cancelnewhn,
                                "patientcometype": listgroupname,
                                "allcounter": results.rows[i].allcounter,
                                "refnoxx": refnoxx,
                                "kiosk_location": req.body.kiosk_location,
                                "counterpayor": (results.rows[i].counterpayor == null) ? '' : results.rows[i].counterpayor,
                            }
                            aaa.push(dddd);

                            global.endkiosk.push(dddd);

                            //console.log(aaa, "DATA AAAAAAAAAA")

                            flowname = results.rows[i].flowname;
                            groupprocessuid = results.rows[i].groupprocessuid;
                            queueno = results.rows[i].queueno;
                            if (results.rows[i].prename == null) {
                                results.rows[i].prename = '';
                            }
                            if (results.rows[i].forename == null) {
                                results.rows[i].forename = '';
                            }
                            if (results.rows[i].surname == null) {
                                results.rows[i].surname = '';
                            }
                            fullname = results.rows[i].prename + ' ' + results.rows[i].forename + ' ' + results.rows[i].surname;
                            birthdate = results.rows[i].dob;
                            regiswhen = dateFormat(results.rows[i].patient_cwhen, "HH:MM" + " น.");
                            patientuid = results.rows[i].patientuid;
                            citizenid = results.rows[i].idcard;
                            refnoxx = results.rows[i].refno;
                            hn = results.rows[i].hn;
                            var infoData = { location: '7', fullname: fullname, dob: birthdate, hn: hn, idcard: citizenid, regiswhen: regiswhen }
                            //console.loginfoData, 'test infodata no idcard')
                        }
                        // var rrr ={location:7,data:dddd}
                        // global.endkiosk.push(rrr);
                        if (sql_value != "") {
                            var new_values = sql_value.substr(1);
                            var sql = "INSERT INTO tr_processcontrol (patientuid,worklistuid,createdate,queueno) VALUES " + new_values + " RETURNING *";
                            //console.logsql, "INSERT PROCESSCONTROL");
                            pool.query(sql, (err, results) => {
                                if (!err) {
                                    //console.logresults.rows, "TEST")
                                }
                            });
                        }


                        global.io.emit('redirect_slip', { step: 'next7', kiosk_location: req.body.kiosk_location });
                        var urlqrcode = '';
                        //console.logpatientuid, "PATIENTUID FOR GEN QRCODE")
                        //console.logcitizenid, "IDCARD FOR GEN QRCODE")

                        axios({
                            method: 'post',
                            url: `${config['genSession']}/dev/generatesession`,
                            headers: {
                                "Content-Type": "application/json"
                            },
                            data: { pid: refnoxx, patientuid: patientuid }
                        }).then(function (response) {
                            //console.log(response, "DATA QR CODE");
                            urlqrcode = response.data.urlqrcode;
                            //console.logurlqrcode, "URL QR CODE")
                            // callback({ success: true, data: response.data });
                            //console.logurlqrcode, "QR CODE COMPLETE")
                            var d = new Date();
                            var now = new Date(d.getFullYear() + 543, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
                            var datetime = dateFormat(now, "dd/mm/yyyy HH:MM" + " น.");
                            var printQueue = { "ref": patientuid, "fullname": fullname, "identification": citizenid, "birthdate": birthdate, "hn": hn, "service_step": aaa, "timestamp": datetime, "regiswhen": regiswhen, "qrcode": urlqrcode, "patientcometype": listgroupname, "refnoxx": refnoxx };
                            // //console.log(printQueue, "PRINT QUEUE WITH QRCODE")
                            var jsonString = JSON.stringify(printQueue);
                            pool.query("INSERT INTO tr_printqueuelist (printloglist) VALUES ('" + jsonString + "') RETURNING uid", (err, results) => {
                                if (!err) {
                                    axios({
                                        method: 'post',
                                        url: `${config['deviceAPI']}/print`,
                                        headers: {
                                            "Content-Type": "application/x-www-form-urlencoded"
                                        },
                                        data: qs.stringify({ "printid": results.rows[0].uid, "locationid": req.body.kiosk_location })
                                    });
                                    axios({
                                        method: 'post',
                                        url: `${config['selfURL']}/api/manage/get_new_queue`,
                                        headers: {
                                            "Access-Control-Allow-Origin": "*",
                                            "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
                                            "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
                                            "Authorization": "Basic YXJtOjEyMzQ=",
                                        },
                                        data: { queueno: Queueno },
                                    });

                                    // ////console.logresults.rows[0].uid);
                                }
                            });
                        }).catch(function (error) {
                            console.log(error);
                            if (typeof error.response == 'undefined' || error.response.status == 404) {

                                // //console.log(response, "DATA QR CODE");
                                // urlqrcode = response.data.urlqrcode;
                                //console.logurlqrcode, "URL QR CODE")
                                // callback({ success: true, data: response.data });
                                //console.logurlqrcode, "QR CODE COMPLETE")
                                var d = new Date();
                                var now = new Date(d.getFullYear() + 543, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
                                var datetime = dateFormat(now, "dd/mm/yyyy HH:MM" + " น.");
                                var printQueue = { "ref": patientuid, "fullname": fullname, "identification": citizenid, "birthdate": birthdate, "hn": hn, "service_step": aaa, "timestamp": datetime, "regiswhen": regiswhen, "qrcode": '', "patientcometype": listgroupname, "refnoxx": refnoxx };
                                // //console.log(printQueue, "PRINT QUEUE WITH QRCODE")
                                var jsonString = JSON.stringify(printQueue);
                                pool.query("INSERT INTO tr_printqueuelist (printloglist) VALUES ('" + jsonString + "') RETURNING uid", (err, results) => {
                                    if (!err) {
                                        axios({
                                            method: 'post',
                                            url: `${config['deviceAPI']}/print`,
                                            headers: {
                                                "Content-Type": "application/x-www-form-urlencoded"
                                            },
                                            data: qs.stringify({ "printid": results.rows[0].uid, "locationid": req.body.kiosk_location })
                                        });
                                        axios({
                                            method: 'post',
                                            url: `${config['selfURL']}/api/manage/get_new_queue`,
                                            headers: {
                                                "Access-Control-Allow-Origin": "*",
                                                "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
                                                "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
                                                "Authorization": "Basic YXJtOjEyMzQ=",
                                            },
                                            data: { queueno: Queueno },
                                        });
                                        // ////console.logresults.rows[0].uid);
                                    }
                                });
                            }
                        });

                    }
                }
            });
        } else if (req.body.worklistgroup != '' && req.body.Queueno == '' && req.body.Categoryuid == '') {
            var data = req.body;
            var dataInfo = '';
            if (req.body.info) {
                dataInfo = JSON.parse(req.body.info);
                // //console.logdataInfo, 'INFO');
            }
            //console.log(data, 'Data Print Slip 1');

            //var sql ="select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,max(queueno) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title from tr_patient c LEFT JOIN tr_patientqueue a ON c.uid = a.patientuid left join vw_worklistprocess b on(b.worklistgroup = '" + data.worklistgroup + "' and(b.groupprocessuid = a.groupprocessuid or b.groupprocessuid is null)) where c.uid = '" + data.Patientuid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle Order by worklistuid"
            var sql = "select distinct max(c.uid) as patientuid, max(c.clinicname) as clinicname, max(refno) as refno, max(allcounter) as allcounter, min(c.cwhen) as patient_cwhen,min(a.cwhen) as cwhen,worklistuid,max(b.groupprocessuid) as groupprocessuid,(select queueno from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and tr_patientqueue.groupprocessuid = max(b.groupprocessuid) limit 1) as queueno,max(call_location) as call_location,max(worklistgroup) as worklistgroup,max(worklistgroupname) as worklistgroupname,max(processname) as processname,max(flowname) as flowname,max(prename) as prename,max(forename) as forename, max(surname) as surname,max(idcard) as idcard,max(hn) as hn,max(dob) as birthdate,(case when b.worklistuid = 1 then 1 else null end) closeselectone,(case when b.worklistuid = 2 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 11 limit 1) else null end) closevs,(case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 10 limit 1) else null end) closenewhn, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 9  limit 1) else null end) closepayor, CASE WHEN b.worklistuid = 3 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp WHERE pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen AND pp.groupprocessuid = 1 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid =1 limit 1) = pp.queuecategoryuid AND pp.closqueue_registerhn IS NULL AND pp.callnewhn IS NULL AND pp.cancelqueue_registerhn is null) ELSE NULL::bigint END AS count_patient_newhn, CASE WHEN b.worklistuid = 5 THEN ( SELECT count(*) AS count FROM vw_patientqueue pp where groupprocessuid = 2 and (select queuecategoryuid from tr_patientqueue where tr_patientqueue.patientuid = a.patientuid and groupprocessuid = 2 limit 1) = pp.queuecategoryuid AND pp.active::text = 'Y'::text AND pp.cwhen <= a.cwhen  AND pp.closqueue_register IS NULL AND pp.callqueue IS NULL AND pp.cancelqueue_register is null) ELSE NULL::bigint END AS count_patient_payor, (case when b.worklistuid = 5 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 14) else null end) cancelregister, (case when b.worklistuid = 3 then(select createdate from tr_processcontrol pcc where pcc.patientuid = a.patientuid and pcc.worklistuid = 15) else null end) cancelnewhn, b.tltle as title, ( select tb_payor.counter from tb_payor where tb_payor.uid = c.payorid::integer limit 1 ) as counterpayor from vw_worklistprocess b LEFT outer JOIN tr_patient c on b.worklistgroup = '" + data.worklistgroup + "' left outer join tr_patientqueue a ON c.uid = a.patientuid where c.uid = '" + data.Patientuid + "' group by worklistuid, a.patientuid, a.cwhen, b.tltle,a.groupprocessuid,c.payorid Order by worklistuid";
            // //console.logsql, "NO QUEUE")
            pool.query(sql, (err, results) => {
                if (!err) {
                    if (results.rowCount > 0) {
                        //console.logresults.rows, "TEST SELECT QUERY")
                        var aaa = [];
                        var sql_value = "";
                        var fullname = "";
                        var birthdate = "";
                        var patientuid = "";
                        var citizenid = "";
                        var hn = "";
                        var regiswhen = "";
                        var flowname = '';

                        clearKioskLocation(req.body.kiosk_location);

                        for (var i = 0; i < results.rows.length; i++) {
                            var flownameRoom = "";
                            sql_value = sql_value + ",('" + results.rows[i].patientuid + "','" + results.rows[i].worklistuid + "',now())";

                            //console.log(results.rows[i], "TEST DATA RE PRINT NO QUEUE NUMBER")
                            if (results.rows[i].worklistuid == 6 && results.rows[i].clinicname != '') {
                                flownameRoom = ' ' + results.rows[i].clinicname;//.replace(".",",");
                            }
                            //console.log(flownameRoom, "TEST CLINIC NAME NO QUEUENUMBER");
                            var inFoData2 = '';
                            if (dataInfo.data2) {
                                inFoData2 = dataInfo.data2;
                            }
                            results.rows[i].allcounter = (results.rows[i].worklistuid == 5) ? results.rows[i].counterpayor : results.rows[i].allcounter;
                            var dddd = {
                                "location": '7',
                                "sequence": i + 1,
                                "title": results.rows[i].title,
                                "flowname": results.rows[i].flowname + flownameRoom,
                                "waitinghn": "",
                                "waitingpayor": "",
                                "queueno": "",
                                "worklistuid": results.rows[i].worklistuid,
                                "groupprocessuid": "",
                                "infoData": inFoData2,
                                "group": results.rows[i].worklistgroup,
                                "closeselectone": results.rows[i].closeselectone,
                                "closevs": results.rows[i].closevs,
                                "closenewhn": results.rows[i].closenewhn,
                                "closepayor": results.rows[i].closepayor,
                                "count_patient_newhn": results.rows[i].count_patient_newhn,
                                "count_patient_payor": results.rows[i].count_patient_payor,
                                "cancelregister": results.rows[i].cancelregister,
                                "cancelnewhn": results.rows[i].cancelnewhn,
                                "patientcometype": listgroupname,
                                "allcounter": results.rows[i].allcounter,
                                "refnoxx": refnoxx,
                                "kiosk_location": req.body.kiosk_location,
                                "counterpayor": (results.rows[i].counterpayor == null) ? '' : results.rows[i].counterpayor,
                            }
                            aaa.push(dddd);

                            global.endkiosk.push(dddd);



                            flowname = results.rows[i].flowname;
                            if (results.rows[i].prename == null) {
                                results.rows[i].prename = '';
                            }
                            if (results.rows[i].forename == null) {
                                results.rows[i].forename = '';
                            }
                            if (results.rows[i].surname == null) {
                                results.rows[i].surname = '';
                            }
                            fullname = results.rows[i].prename + ' ' + results.rows[i].forename + ' ' + results.rows[i].surname;
                            birthdate = results.rows[i].dob;
                            regiswhen = dateFormat(results.rows[i].patient_cwhen, "HH:MM" + " น.");
                            patientuid = results.rows[i].patientuid;
                            citizenid = results.rows[i].idcard;
                            hn = results.rows[i].hn;
                            refnoxx = results.rows[i].refno;
                            var infoData = { location: '7', fullname: fullname, dob: birthdate, hn: hn, idcard: citizenid, regiswhen: regiswhen }
                            //console.loginfoData, 'test infodata no idcard')
                        }
                        // var rrr ={location:7,data:dddd}
                        // global.endkiosk.push(rrr);
                        if (sql_value != "") {
                            var new_values = sql_value.substr(1);
                            var sql = "INSERT INTO tr_processcontrol (patientuid,worklistuid,createdate) VALUES " + new_values + " RETURNING *";
                            //console.logsql, "INSERT PROCESSCONTROL");
                            pool.query(sql, (err, results) => {
                                if (!err) {
                                    //console.logresults.rows, "TEST")
                                }
                            });
                        }


                        global.io.emit('redirect_slip', { step: 'next7', kiosk_location: req.body.kiosk_location });
                        var urlqrcode = '';
                        //console.logpatientuid, "PATIENTUID FOR GEN QRCODE")
                        //console.logcitizenid, "IDCARD FOR GEN QRCODE")
                        axios({
                            method: 'post',
                            url: `${config['genSession']}/dev/generatesession`,
                            headers: {
                                "Content-Type": "application/json"
                            },
                            data: { pid: refnoxx, patientuid: patientuid }
                        }).then(function (response) {
                            //console.logresponse, "DATA QR CODE");
                            urlqrcode = response.data.urlqrcode;
                            //console.logurlqrcode, "URL QR CODE")
                            // callback({ success: true, data: response.data });
                            //console.logurlqrcode, "QR CODE COMPLETE")
                            var d = new Date();
                            var now = new Date(d.getFullYear() + 543, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
                            var datetime = dateFormat(now, "dd/mm/yyyy HH:MM" + " น.");
                            var printQueue = { "ref": patientuid, "fullname": fullname, "identification": citizenid, "birthdate": birthdate, "hn": hn, "service_step": aaa, "timestamp": datetime, "regiswhen": regiswhen, "qrcode": urlqrcode, "patientcometype": listgroupname, "refnoxx": refnoxx };
                            // //console.logprintQueue, "PRINT QUEUE WITH QRCODE")
                            var jsonString = JSON.stringify(printQueue);
                            pool.query("INSERT INTO tr_printqueuelist (printloglist) VALUES ('" + jsonString + "') RETURNING uid", (err, results) => {
                                if (!err) {
                                    axios({
                                        method: 'post',
                                        url: `${config['deviceAPI']}/print`,
                                        headers: {
                                            "Content-Type": "application/x-www-form-urlencoded"
                                        },
                                        data: qs.stringify({ "printid": results.rows[0].uid, "locationid": req.body.kiosk_location })
                                    });
                                    axios({
                                        method: 'post',
                                        url: `${config['selfURL']}/api/manage/get_new_queue`,
                                        headers: {
                                            "Access-Control-Allow-Origin": "*",
                                            "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
                                            "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
                                            "Authorization": "Basic YXJtOjEyMzQ=",
                                        },
                                        data: { queueno: Queueno },
                                    });

                                    // ////console.logresults.rows[0].uid);
                                }
                            });
                        }).catch(function (error) {
                            if (error.response.status == 404) {

                                var d = new Date();
                                var now = new Date(d.getFullYear() + 543, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
                                var datetime = dateFormat(now, "dd/mm/yyyy HH:MM" + " น.");
                                var printQueue = { "ref": patientuid, "fullname": fullname, "identification": citizenid, "birthdate": birthdate, "hn": hn, "service_step": aaa, "timestamp": datetime, "regiswhen": regiswhen, "qrcode": '', "patientcometype": listgroupname, "refnoxx": refnoxx };
                                // //console.log(printQueue, "PRINT QUEUE WITH QRCODE")
                                var jsonString = JSON.stringify(printQueue);
                                pool.query("INSERT INTO tr_printqueuelist (printloglist) VALUES ('" + jsonString + "') RETURNING uid", (err, results) => {
                                    if (!err) {
                                        axios({
                                            method: 'post',
                                            url: `${config['deviceAPI']}/print`,
                                            headers: {
                                                "Content-Type": "application/x-www-form-urlencoded"
                                            },
                                            data: qs.stringify({ "printid": results.rows[0].uid, "locationid": req.body.kiosk_location })
                                        });
                                        axios({
                                            method: 'post',
                                            url: `${config['selfURL']}/api/manage/get_new_queue`,
                                            headers: {
                                                "Access-Control-Allow-Origin": "*",
                                                "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
                                                "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
                                                "Authorization": "Basic YXJtOjEyMzQ=",
                                            },
                                            data: { queueno: Queueno },
                                        });

                                        // ////console.logresults.rows[0].uid);
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }

    }




    // ////console.logindex,'process');
    // res.status(200).send({success:index});


});

router.post('/getSlip', (req, res) => {
    if (req.body.Slip == '1' && req.body.location == '7' && req.body.step == 'next7') {
        // //console.logglobal.endkiosk, "SLIP");
        const arr2 = global.endkiosk.filter((d) => {
            return d.kiosk_location == req.body.kiosk_location;
        });
        //console.logarr2, "TEST FILTER arr2")
        console.log('print naja');
        if (arr2[0].location == '8') {
            console.log('print 8');
            var aaa = [];
            var patientuid = '';
            var fullname = "";
            var birthdate = "";
            var patientuid = "";
            var queueno = "";
            var citizenid = "";
            var hn = "";
            var prename = '';
            var forename = '';
            var surname = '';
            var regiswhen = '';
            var listgroupname = '';
            var refnoxx = '';
            for (var i = 0; i < arr2.length; i++) {
                var flownameRoom = '';

                //console.logarr2[i], "SLIP 5");
                if (arr2[i].infoData.worklistgroup == '1') {
                    listgroupname = 'Walk-in';
                } else if (arr2[i].infoData.worklistgroup == '2') {
                    listgroupname = 'นัดหมาย';
                } else if (arr2[i].infoData.worklistgroup == '3') {
                    listgroupname = 'ผู้ป่วยใหม่';
                } else if (arr2[i].infoData.worklistgroup == '4') {
                    listgroupname = 'Walk-in';
                } else if (arr2[i].infoData.worklistgroup == '5') {
                    listgroupname = 'นัดหมาย';
                } else if (arr2[i].infoData.worklistgroup == '6') {
                    listgroupname = 'นัดหมาย';
                } else if (arr2[i].infoData.worklistgroup == '7') {
                    listgroupname = 'นัดหมาย';
                } else if (arr2[i].infoData.worklistgroup == '8') {
                    listgroupname = 'Walk-in';
                } else if (arr2[i].infoData.worklistgroup == '9') {
                    listgroupname = 'นัดหมาย';
                } else if (arr2[i].infoData.worklistgroup == '10') {
                    listgroupname = 'นัดหมาย';
                }

                if (arr2[i].infoData.worklistuid == 6 && arr2[i].infoData.clinicname != '') {
                    flownameRoom = ' ' + arr2[i].clinicname;//.replace(".",",");
                }
                // //console.logarr2[i], "TEST ENDKIOSK LOOP")

                arr2[i].allcounter = (arr2[i].worklistuid == 5) ? arr2[i].counterpayor : arr2[i].allcounter;

                var dddd = {
                    "location": '7',
                    "sequence": arr2[i].sequence,
                    "title": arr2[i].title,
                    "flowname": arr2[i].flowname + flownameRoom,
                    "waitinghn": arr2[i].waitinghn,
                    "waitingpayor": arr2[i].waitingpayor,
                    "queueno": (arr2[i].groupprocessuid ? arr2[i].queueno : ''),
                    "group": arr2[i].worklistgroup,
                    "closeselectone": arr2[i].closeselectone,
                    "closevs": arr2[i].closevs,
                    "closenewhn": arr2[i].closenewhn,
                    "closepayor": arr2[i].closepayor,
                    "count_patient_newhn": arr2[i].count_patient_newhn,
                    "count_patient_payor": arr2[i].count_patient_payor,
                    "cancelregister": arr2[i].cancelregister,
                    "cancelnewhn": arr2[i].cancelnewhn,
                    "worklistuid": arr2[i].worklistuid,
                    "groupprocessuid": arr2[i].groupprocessuid,
                    "patientcometype": listgroupname,
                    "allcounter": arr2[i].allcounter,
                    "refnoxx": arr2[i].refnoxx,
                    "kiosk_location": req.body.kiosk_location,
                    "counterpayor": (arr2[i].counterpayor == null) ? '' : arr2[i].counterpayor,
                }
                aaa.push(dddd);
                //console.log(arr2[i], "TEST DATA RE PRINT !!!!!!!");

                // flowname = arr2[i].flowname;
                groupprocessuid = arr2[i].groupprocessuid;
                queueno = arr2[i].queueno;
                if (arr2[i].infoData.prefixname_th != null || arr2[i].infoData.prefixname_th != '') {
                    prename = arr2[i].infoData.prefixname_th;
                }
                if (arr2[i].infoData.firstname_th != null || arr2[i].infoData.firstname_th != '') {
                    forename = arr2[i].infoData.firstname_th;
                }
                if (arr2[i].infoData.lastname_th != null || arr2[i].infoData.lastname_th != '') {
                    surname = arr2[i].infoData.lastname_th;
                }

                fullname = prename + ' ' + forename + ' ' + surname;
                refnoxx = arr2[i].infoData.refnoxx;
                birthdate = arr2[i].infoData.dob;
                regiswhen = dateFormat(arr2[i].infoData.patient_cwhen, "HH:MM" + " น.");
                patientuid = arr2[i].infoData.patient_uid;
                citizenid = arr2[i].infoData.idcard;
                hn = arr2[i].infoData.hnno;
                // var infoData = { location: '7', fullname: fullname, dob: birthdate, hn: hn, idcard: citizenid, regiswhen: regiswhen }
            }
            var urlqrcode = '';
            //console.logpatientuid, "PATIENTUID FOR GEN QRCODE")
            //console.logcitizenid, "IDCARD FOR GEN QRCODE")
            axios({
                method: 'post',
                url: `${config['genSession']}/dev/generatesession`,
                headers: {
                    "Content-Type": "application/json"
                },
                data: { pid: refnoxx, patientuid: patientuid }
            }).then(function (response) {
                // //console.logresponse, "DATA QR CODE");
                urlqrcode = response.data.urlqrcode;
                //console.logurlqrcode, "URL QR CODE")
                // callback({ success: true, data: response.data });
                //console.logurlqrcode, "QR CODE COMPLETE")
                var d = new Date();
                var now = new Date(d.getFullYear() + 543, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
                var datetime = dateFormat(now, "dd/mm/yyyy HH:MM" + " น.");
                var printQueue = { "ref": patientuid, "fullname": fullname, "identification": citizenid, "birthdate": birthdate, "hn": hn, "service_step": aaa, "timestamp": datetime, "regiswhen": regiswhen, "qrcode": urlqrcode, "patientcometype": listgroupname, "refnoxx": refnoxx };
                //console.logprintQueue, "TEST PRINT RE SLIP DATA")
                var jsonString = JSON.stringify(printQueue);
                pool.query("INSERT INTO tr_printqueuelist (printloglist) VALUES ('" + jsonString + "') RETURNING uid", (err, results) => {
                    if (!err) {
                        axios({
                            method: 'post',
                            url: `${config['deviceAPI']}/print`,
                            headers: {
                                "Content-Type": "application/x-www-form-urlencoded"
                            },
                            data: qs.stringify({ "printid": results.rows[0].uid, "locationid": req.body.kiosk_location })
                        });
                        axios({
                            method: 'post',
                            url: `${config['selfURL']}/api/manage/get_new_queue`,
                            headers: {
                                "Access-Control-Allow-Origin": "*",
                                "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
                                "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
                                "Authorization": "Basic YXJtOjEyMzQ=",
                            },
                            data: { queueno: queueno },
                        });

                        // ////console.logresults.rows[0].uid);
                    }
                });
                global.io.emit('slip_station', { data: arr2, kiosk_location: req.body.kiosk_location });
                res.status(200).send({ data: arr2, kiosk_location: req.body.kiosk_location })
                clearKioskLocation(req.body.kiosk_location);
            }).catch(function (error) {
                if (typeof error.response === 'undefined' || (typeof error.response !== 'undefined' && error.response.status == 404)) {

                    var d = new Date();
                    var now = new Date(d.getFullYear() + 543, d.getMonth(), d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds())
                    var datetime = dateFormat(now, "dd/mm/yyyy HH:MM" + " น.");
                    var printQueue = { "ref": patientuid, "fullname": fullname, "identification": citizenid, "birthdate": birthdate, "hn": hn, "service_step": aaa, "timestamp": datetime, "regiswhen": regiswhen, "qrcode": '', "patientcometype": listgroupname, "refnoxx": refnoxx };
                    // //console.log(printQueue, "PRINT QUEUE WITH QRCODE")
                    var jsonString = JSON.stringify(printQueue);
                    pool.query("INSERT INTO tr_printqueuelist (printloglist) VALUES ('" + jsonString + "') RETURNING uid", (err, results) => {
                        if (!err) {
                            axios({
                                method: 'post',
                                url: `${config['deviceAPI']}/print`,
                                headers: {
                                    "Content-Type": "application/x-www-form-urlencoded"
                                },
                                data: qs.stringify({ "printid": results.rows[0].uid, "locationid": req.body.kiosk_location })
                            });
                            axios({
                                method: 'post',
                                url: `${config['selfURL']}/api/manage/get_new_queue`,
                                headers: {
                                    "Access-Control-Allow-Origin": "*",
                                    "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
                                    "Access-Control-Allow-Headers": "Authorization, Origin, X-Requested-With, Content-Type, Accept",
                                    "Authorization": "Basic YXJtOjEyMzQ=",
                                },
                                data: { queueno: Queueno },
                            });

                            // ////console.logresults.rows[0].uid);
                        }
                    });
                    global.io.emit('slip_station', { data: arr2, kiosk_location: req.body.kiosk_location });
                    res.status(200).send({ data: arr2, kiosk_location: req.body.kiosk_location })
                    clearKioskLocation(req.body.kiosk_location);
                }
            });
        } else if (arr2[0].location == '7') {
            console.log('print 7');
            global.io.emit('slip_station', { data: arr2, kiosk_location: req.body.kiosk_location });
            res.status(200).send({ data: arr2, kiosk_location: req.body.kiosk_location })
            clearKioskLocation(req.body.kiosk_location);
        }




    }
})

var clearKioskLocation = (param) => {
    const arr1 = global.endkiosk.filter((d) => {
        return d.kiosk_location != param;
    });
    global.endkiosk = arr1;
}


module.exports = router;