const router = require('express').Router();
const { Pool } = require('pg');
const findIndex = require('../function/findIndex');
//const config = require('./../function/connect');

const axios = require('axios');
const pool = new Pool(config['dbconfig']);



router.post('/get_new_queue',(req, res) => {
    //console.logreq.body, 'test send manage1')
    global.io.emit('manage_getqueue_row', {data: req.body});
    res.status(200).send({success: true});
});

// router.post('/queueno', (req, res) => {
//     if(req.body.requestQueueNo == '1'){
//         //console.logreq.body, 'test send manage');
//         var queryQueueCategory = "SELECT * FROM vw_patientqueue WHERE queuecategoryuid='"+req.body.queuecategory+"' AND queueno != 'null'";
//         pool.query(queryQueueCategory, (err, results) => {
//             if(!err){
//                 global.io.emit('manage_getqueue', {data: results.rows});
//             }
//         });
//     }else{
//         res.status(200).send({success:false});
//     }
// });

router.post('/call_queue', (req, res) => {
    //console.log"GET CALL QUEUE");
    if(req.body.requestCall == '1'){
        //console.log"Request Complete");
        //console.logreq.body.queueno, 'CALL QUEUE');
        global.io.emit('update_action_call', {cateid:req.body.queuecategory,queueno:req.body.queueno,counter:req.body.counter});
 

        res.status(200).send({success:true});
    }else{
        res.status(200).send({success:false});
    }
});

router.post('/hold_queue', (req, res) => {
    //console.log"GET HOLD QUEUE");
    if(req.body.requestHold == '1'){
        //console.log"Request Complete");
        //console.logreq.body.queueno, 'HOLD QUEUE');
        global.io.emit('update_action_hold', {cateid:req.body.queuecategory,queueno:req.body.queueno,counter:req.body.counter});

        res.status(200).send({success:true});
    }else{
        res.status(200).send({success:false});
    }
});

router.post('/note_queue', (req, res) => {
    if(req.body.requestNote == '1'){
        //global.io.emit('update_queue_action', {cateid:req.body.queuecategory,queueno:req.body.queueno});
        global.io.emit('update_action_note', {cateid:req.body.queuecategory,queueno:req.body.queueno});
        //console.logreq.body, 'NOTE QUEUE')

        res.status(200).send({success:true});
    }else{
        res.status(200).send({success:false});
    }
});

router.post('/complete_queue', (req, res) => {
    if(req.body.requestComplete == '1'){
        global.io.emit('remove_queue', {cateid:req.body.queuecategory,queueno:req.body.queueno,worklistuid:req.body.worklistuid});
        //console.logreq.body, 'COMPLETE QUEUE')

        res.status(200).send({success:true});
    }else{
        res.status(200).send({success:false});
    }
});

router.post('/close_queue', (req, res) => {
    if(req.body.requestClose == '1'){
        global.io.emit('remove_queue', {cateid:req.body.queuecategory,queueno:req.body.queueno,worklistuid:req.body.worklistuid});
        //console.logreq.body, 'CLOSE QUEUE')

        res.status(200).send({success:true});
    }else{
        res.status(200).send({success:false});
    }
});

router.post('/filter_insert', (req, res) => {
    if(req.body.requestFilter == '1'){
        //console.logreq.body);
        var refno = req.body.refno;
        var cuser = req.body.cuser;
        pool.query("SELECT uid FROM tr_patient WHERE uid='"+refno+"'", (err,results) => {
            if(!err){
                var patientuid = '';
                patientuid = results.rows[0].uid;
                pool.query("INSERT INTO tr_processcontrol (patientuid,worklistuid,queueno,createdate,cuser) SELECT '"+patientuid+"' as patientuid,'11',(SELECT queueno FROM tr_patientqueue WHERE tr_patient.uid = tr_patientqueue.uid LIMIT 1),now() as createdate,'"+cuser+"' as cuser FROM tr_patient WHERE uid = '"+patientuid+"' LIMIT 1 RETURNING patientuid", (err,results) => {
                    // //console.logresults.rows[0].queueno);
                    //global.io.emit('manage_getqueue_row', {data:{queueno:results.rows[0].queueno}});
                    global.io.emit('manage_getqueue_puid', {success:true});
                    res.status(200).send({success:true,data:'INSERT COMPLETE'});
                })
            }
        });
    }
});

router.get('/refreshClinicCount', (req, res) => {
    global.io.emit('refresh_cliniccount',{success:true});
    res.status(200).send({success:true});
    return true;

});

module.exports = router;