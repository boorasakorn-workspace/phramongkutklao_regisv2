const router = require('express').Router();
const { Pool } = require('pg');
const findIndex = require('./../function/findIndex');
//const config = require('./../function/connect');
const axios = require('axios');
const pool = new Pool(config['dbconfig']);
var dateFormat = require('dateformat');



router.post('/appointment', (req, res) => {
    
    var index = findIndex(5);
    if(index != -1){
        global.kiosk.splice(index, 1);
    }
    
    
    // ////console.log(req.body), 'test appoint';
    if(req.body.requestAP == '1'){
    // ////console.log(req.body), 'test appoint';
        res.status(200).send({success:true,step:'next6', kiosk_location: req.body.kiosk_location });
    }else{
        ////console.log(req.body, "TEST APP");
        res.status(200).send({success:true,step:'next3', kiosk_location: req.body.kiosk_location });
    }
});




var getAppointmentDataAPI = (param, callback) => {
    var productionurl = `${config['hlabAPI']}/ords/pmkords/hlab/appointment/${param.dateappointment}/null/${param.hnnumber}`;
    axios({
        method: 'get',
        url: productionurl,
        headers: {
            "Content-Type": "application/json"
        },

    }).then(function (response) {
        ////console.log(response.data, "APPOINTMENT")
        if (response.data) {
            callback({ success: true, data: response.data });
        } else {
            callback({ success: true, data: '' });
        }
    }).catch(error => {
        if(error.response.status == 404){
            callback({ success:true, data: '' });
        }else if(error.response.status == 500){
            callback({ success:true, error: 500 });
        }
        // ////console.log(err.response.status, "ERROR API FROM APPOINTMENT WITH INPUT")
    });
}



module.exports = router;