const router = require('express').Router();
const { Pool } = require('pg');
//const config = require('./../function/connect');
var dateFormat = require('dateformat');

const axios = require('axios');
const pool = new Pool(config['dbconfig']);
const fs = require('fs');

//

router.post('/open/credit', (req, res) => {
    // console.log("TEST ROUTE CREDIT")
    if (req.body.postCredit == 1) {
        var p_run_hn = null;
        var p_year_hn = null;
        var p_opd = null;
        var p_credit_id = null;
        var p_patient_type_id = null;
        var p_event_no = null;
        var p_user_created = "KIOSK";
        var p_pid = null;
        var hn = req.body.hn;
        var payor_auto = req.body.payor_auto;
        var group = req.body.worklistgroup;
        if (req.body.p_opd != '') {
            p_opd = req.body.p_opd;
        }
        if (req.body.p_credit_id != '') {
            p_credit_id = req.body.p_credit_id;
        }
        if (req.body.p_patient_type_id != '') {
            p_patient_type_id = req.body.p_patient_type_id;
        }
        if (req.body.pid) {
            p_pid = req.body.pid;
        }
        if (hn != '' || hn != undefined || hn != null) {
            if (hn.includes('/')) {
                var s = hn.split('/');
                p_run_hn = s[0];
                p_year_hn = s[1];
                var c = { p_run_hn: p_run_hn, p_year_hn: p_year_hn, p_opd: p_opd, p_credit_id: p_credit_id, p_patient_type_id: p_patient_type_id, p_event_no: p_event_no, p_user_created: p_user_created };
                console.log(c);
                postCreditAPI({ p_run_hn: p_run_hn, p_year_hn: p_year_hn, p_opd: p_opd, p_credit_id: p_credit_id, p_patient_type_id: p_patient_type_id, p_event_no: p_event_no, p_user_created: p_user_created }, (res_postcredit) => {
                    // console.log(res_postcredit, "TEST RESPONSE POST CREDIT")
                    if (res_postcredit.success) {
                        // console.log('1');
                        res.status(201).send({ success: true, data: res_postcredit.data })
                    } else {
                        // console.log('2');
                        res.status(201).send({ success: false, data: res_postcredit.status })
                    }
                });
            }
        } else {

        }
    }
});

// var openVisitAPI = (param, callback) => {
//     console.log(param, "PARAMETER OPEN VISIT")
//     if(param.p_run_hn && param.p_year_hn){
//         fs.readFile('./mockupjson/openvisit.json', 'utf8', (err, dataJson) => {
//             if (err) {
//                 console.log("File read failed:", err)
//                 callback({success: true, data: ''})
//             }
//             var aaa = JSON.parse(dataJson.toString());
//             const arr2 = aaa.filter((d) => {
//                 return d.p_run_hn == param.p_run_hn && d.p_year_hn == param.p_year_hn;
//             });
//             console.log(arr2, "TEST DATA openvisit")

//             if(arr2.length > 0){
//                 callback({success: true, data: arr2});
//             }else{
//                 // Created Visit
//             }
//             // if(arr2.length){
//             //     callback({success: true, data: arr2})
//             // }else{
//             //     callback({success: true, data: '', data1: ''})
//             // }

//         });
//     }


// }




// }

var postCreditAPI = (param, callback) => {
    console.log(param);
    var productionurl = `${config['selfURL']}/ords/pmkords/hlab/credit/`;
    axios({
        method: 'post',
        url: productionurl,
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "POST, GET, OPTIONS",
            "Access-Control-Allow-Headers": "*",
            "Authorization": "Basic YXJtOjEyMzQ",
        },
        data: param,

    }).then(response => {
        
        //console.log(response, "DATA POST")
        //console.log(response.data, "RESPONSE POST CREDIT")
        if (response.data.success && response.status == 200 || response.status == 201) {
            callback({ success: true, status: 201, data: response.data })
        } else {
            callback({ success: false, status: 201, data: '' })
        }
        // if (response.data) {
        //     callback({ success: true, status: 200, data: response.data });
        // }
    }).catch(error => {

        //console.log(error.response.status, "RESPONSE ERROR CREDIT")
        if (error.response.status == 403) {
            callback({ success: false, status: 403, data: '' });
        } else if (error.response.status == 404) {
            callback({ success: false, status: 404, data: '' });
        } else if (error.response.status == 500) {
            callback({ success: false, status: 500, data: '' });
        }
    });
}

// var openVisitAPI = (param, callback) => {
//     console.log(param, 'parameter post credit')
//     var productionurl = "http://27.254.59.21/ords/pmkords/hlab/visit/";
//     axios({
//         method: 'post',
//         url: productionurl,
//         headers: {
//             "Content-Type": "application/json"
//         },
//         data: param,

//     }).then(response => {
//         console.log(response, "RESPONSE POST CREDIT")
//         if (response.data) {
//             callback({ success: true, status: 200, data: response.data });
//         }
//     }).catch(error => {
//         console.log(error, "RESPONSE ERROR CREDIT")
//         if (error.response.status == 400) {
//             callback({ success: false, status: 400, data: '' });
//         } else if (error.response.status == 404) {
//             callback({ success: false, status: 404, data: '' });
//         } else if (error.response.status == 500) {
//             callback({ success: false, status: 500, data: ''});
//         }
//     });
// }

module.exports = router;