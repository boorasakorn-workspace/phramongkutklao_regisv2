const router = require('express').Router();
var dateFormat = require('dateformat');

const axios = require('axios');
const fs = require('fs');

router.get('/credit/:year/:hn/:ignore?/:datetime?', async (req, res) => {
    if (req.params.year && req.params.hn) {
        //get hn&&year
        let fullHn = `${req.params.hn}/${req.params.year}`;
        let lastPayor = await getLastPayorByHN({hn:fullHn}); //mockup use hn/year format
        await res.status(200).send(lastPayor);
    }
});

const getLastPayorByHN = async (param) => {
    if(param.hn){
        const readFile = await fs.readFileSync('./mockupjson/onsite-pmk/api-currentcredit.json', 'utf8');
        if (readFile) {
            readJSON = await JSON.parse(readFile.toString());
            const arr2 = await readJSON.filter((d) => {
                return d.hn == param.hn;
            });
            if(arr2.length){
                return ({success: true, data: arr2[arr2.length - 1],data1: arr2})
            }else if(arr2.length == 1){
                return ({success: true, data: arr2, data1: arr2})
            }else{
                return ({success: true, data: '', data1: ''})
            }
        }else{
            return ({success: true, data: ''})
        }
    }
}

module.exports = router;