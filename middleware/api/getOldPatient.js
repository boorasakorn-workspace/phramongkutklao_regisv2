const router = require('express').Router();
const { Pool } = require('pg');
//const config = require('./../function/connect');
var dateFormat = require('dateformat');

const axios = require('axios');
const pool = new Pool(config['dbconfig']);
const fs = require('fs');

router.get('/patient/:year/:hnnum/:pid', (req, response) => {
    //console.log(req.params)
    if(req.params.year != 'null' && req.params.hnnum != 'null' && req.params.pid == 'null'){
        syncMockPatientAPI({hn_year: req.params.year, hn_num: req.params.hnnum}, (res_hnpatient) => {
            // //console.log(res_hnpatient, "TEST HN OLD PATIENT")
            if(res_hnpatient.success){
                response.status(200).send({success: true, data: res_hnpatient.data})
            }else{
                response.status(404).send({success: false})
            }
        });
    }else{
        syncMockPatientAPI({pid: req.params.pid}, (res_pidpatient) => {
            //console.log(res_pidpatient, "TEST PID OLD PATIENT")
            if(res_pidpatient.success){
                response.status(200).send({success: true, data: res_pidpatient.data})
            }else{
                response.status(404).send({success: false})
            }
        });
    }
});



var syncMockPatientAPI = (param, callback) => {
    //console.log(param, "PARAMETER PATIENT OLD")
    if(param.hn_year && param.hn_num){
        fs.readFile('./mockupjson/onsite-pmk/api-patient.json', 'utf8', (err, dataJson) => {
            if (err) {
                //console.log("File read failed:", err)
                callback({success: true, data: ''})
            }
            var hn = param.hn_num+'/'+param.hn_year;
            var aaa = JSON.parse(dataJson.toString());
            const arr2 = aaa.filter((d) => {
                return d.hn == hn;
            });

            if(arr2.length){
                callback({success: true, data: arr2[0]})
            }else{
                callback({success: false})
            }
            
        });
    }else if(param.pid){
        fs.readFile('./mockupjson/onsite-pmk/api-patient.json', 'utf8', (err, dataJson) => {
            if (err) {
                //console.log("File read failed:", err)
                callback({success: true, data: ''})
            }
            var aaa = JSON.parse(dataJson.toString());
            const arr2 = aaa.filter((d) => {
                return d.pid == param.pid;
            });
            //console.log(arr2, "TEST DATA JSON")
            if(arr2.length){
                callback({success: true, data: arr2[0]})
            }else{
                callback({success: false})
            }
            
        });
    }
}



module.exports = router;