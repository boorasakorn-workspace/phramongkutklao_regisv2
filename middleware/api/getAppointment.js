const router = require('express').Router();
// const { Pool } = require('pg');
// const config = require('./../function/connect');
var dateFormat = require('dateformat');

const axios = require('axios');
// const pool = new Pool(config);
const fs = require('fs');


router.get('/appointments/:dateappointment/:pid/:hn1/:hn2', (req, response) => {
    console.log(req.params);
    //console.log(req.params, "param for appoint")
    if(req.params.dateappointment != 'null' && req.params.pid == 'null' && req.params.hn1 != 'null' && req.params.hn2 != 'null'){
        syncMockAppointmentAPI({dateappointment: req.params.dateappointment, hn1: req.params.hn1, hn2: req.params.hn2}, (res_hnappointment) => {
            //console.log(res_hnappointment.data, "TEST HN APPOINTMENT")
            
            if(res_hnappointment.success){
                var data = [];
                data.push(res_hnappointment.data);
                response.status(200).send(data)
            }else{
                response.status(404).send({success: false})
            }
        });
    }
});

var syncMockAppointmentAPI = (param, callback) => {
    //console.log(param, "PARAMETER APP OLD")
    if(param.hn1 && param.hn2 && param.dateappointment){
        fs.readFile('./mockupjson/onsite-pmk/api-appointment.json', 'utf8', (err, dataJson) => {
            if (err) {
                //console.log("File read failed:", err)
                callback({success: true, data: ''})
            }
            var hn_num = '';
            // if(param.hn2.length > 2){
            //     hn_num = param.hn1+'/'+param.hn2;
            // }else{
                hn_num = param.hn2+'/'+param.hn1;
            // }
            //console.log(hn_num, "HN NUM");
            var aaa = JSON.parse(dataJson.toString());
            const arr2 = aaa.filter((d) => {
                return d.hn == hn_num && d.date == param.dateappointment;
            });
            // //console.log(arr2[0])
            if(arr2.length){
                callback({success: true, data: arr2[0]})
            }else{
                callback({success: false})
            }
            
        });
    }
}


module.exports = router;