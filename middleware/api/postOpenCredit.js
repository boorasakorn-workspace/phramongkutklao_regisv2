const router = require('express').Router();
// const { Pool } = require('pg');
// const config = require('./../function/connect');
var dateFormat = require('dateformat');

const axios = require('axios');
// const pool = new Pool(config);
const fs = require('fs');

router.post('/credit', (req, res) => {
    //console.log(req.body, "PARAMETER POST CREDIT")
    if (req.body.p_run_hn) {
        axios({
            method: 'post',
            url: `${config['hlabAPI']}/ords/pmkords/hlab/credit/`,
            headers: {
                "Content-Type": "application/json"
            },
            data:req.body,

        }).then(response => {
            var hnnum = req.body.p_run_hn + '/' + req.body.p_year_hn;
            //console.log(response, "OPEN CREDIT ACTIVE")
            var aaa = [];
                const arr2 = aaa.filter((d) => {
                    return d.hn == hnnum && d.policyHolderId == req.body.p_credit_id;
                });
            // callback(aaa);
            if (response.data) {
                res.status(200).send({success: true, data: response.data.data })
            } else {
                res.status(200).send({success: true, data: '' });
            }

        }).catch(error => {
            // //console.log(error.response.status, "ERROR CREDIT INACTIVE")
            if (error.response.status == 404) {
                res.status(200).send({success: false, error: 404 });
            } else if (error.response.status == 403) {
                res.status(200).send({success: false, error: 403 });
            } else if (error.response.status == 500) {
                res.status(200).send({success: false, error: 500 });
            }
        });
    }
});

module.exports = router;